# simple boilerplate

## Boilerplate Collection

Welcome to the Boilerplate Collection! This repository contains a variety of boilerplate setups to kickstart your projects with different frameworks and technologies. Each folder represents a different boilerplate setup.

## Available Boilerplates

1. React Boilerplate
Description: Basic React setup with essential configurations.
Folder: /fast-api-react-vite
2. Vite Boilerplate
Description: Boilerplate for Vite-based projects (Vite + React/Vue/Angular).
Folder: /fast-api-react-vite
3. FastAPI Boilerplate
Description: FastAPI setup with basic configurations for backend development.
Folder: /fast-api-react-vite

(This list will continue to grow with additional boilerplate setups.)

## How to Use

Clone the repository to your local machine:

`
git clone https://gitlab.com/Mohammad-Rahman/simple-boilerplate.git`

Navigate to the desired boilerplate folder:

`
cd simple boilerplate
`

Follow the README instructions within each folder to set up and run the project.

## Contributing

Contributions to this collection are welcome! If you have a boilerplate setup to add, feel free to fork this repository, create a new folder for your boilerplate, and submit a pull request. Ensure the README.md within the new folder contains clear instructions on how to use the boilerplate.
